package edu.westga.cs1302.photogallery.viewmodel;

import java.io.File;
import java.util.ArrayList;

import edu.westga.cs1302.photogallery.model.Photo;
import edu.westga.cs1302.photogallery.model.PhotoAlbumManager;
import edu.westga.cs1302.photogallery.view.resources.Filter;
import edu.westga.cs1302.photogallery.view.resources.ImageFilter;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;

/**
 * The Class PhotoGalleryViewModel.
 * 
 * @author Darrell Branch
 */
public class PhotoGalleryViewModel {
	
	private ListProperty<Photo> photoListProperty;

	private PhotoAlbumManager photoAlbum;
	
	private ObjectProperty<Image> imageProperty;
	
	
	/**
	 * Instantiates a new photo gallery view model.
	 */
	public PhotoGalleryViewModel() {
		this.photoAlbum = new PhotoAlbumManager();
		this.photoListProperty = new SimpleListProperty<Photo>(FXCollections.observableArrayList(this.photoAlbum.getPhotos()));
		this.imageProperty = new SimpleObjectProperty<Image>();
	}
	
	/**
	 * Image property.
	 *
	 * @return the object property
	 */
	public ObjectProperty<Image> imageProperty() {
		return this.imageProperty;
	}
	
	/**
	 * Photo list property.
	 *
	 * @return the list property
	 */
	public ListProperty<Photo> photoListProperty() {
		return this.photoListProperty;
	}
	
	/**
	 * Load photos.
	 *
	 * @param selectedFile the selected file
	 * 
	 * @return success of load
	 */
	public boolean loadPhotos(File selectedFile) {
		boolean result = this.photoAlbum.readPhotoAlbumFile(selectedFile);
		this.photoListProperty.set(FXCollections.observableArrayList(this.photoAlbum.getPhotos()));
		return result;
	}
	
	/**
	 * Gets the duplicates.
	 *
	 * @return the duplicates
	 */
	public ArrayList<String> getDuplicates() {
		return this.photoAlbum.getDuplicateNames();
	}
	
	/**
	 * Removes the photo.
	 *
	 * @param index the index
	 */
	public void removePhoto(int index) {
		Photo photoToRemove = this.photoListProperty.get(index);
		this.photoListProperty.remove(index);
		this.photoAlbum.remove(photoToRemove);
	}
	
	/**
	 * Adds the photo.
	 *
	 * @param selectedFile the selected file
	 */
	public void addPhoto(File selectedFile) {
		Photo newPhoto = new Photo(selectedFile.getAbsolutePath());
		this.photoAlbum.add(newPhoto);
		this.photoListProperty.set(FXCollections.observableArrayList(this.photoAlbum.getPhotos()));
	}
	
	/**
	 * Save album.
	 *
	 * @param selectedFile the selected file
	 */
	public void saveAlbum(File selectedFile) {
		this.photoAlbum.savePhotoAlbum(selectedFile);
	}
	
	/**
	 * Clear gallery.
	 */
	public void clearGallery() {
		this.photoListProperty.set(FXCollections.observableArrayList());
		this.photoAlbum.clear();
	}
	
	/**
	 * Update name.
	 *
	 * @param photo the photo
	 * @param name the name
	 */
	public void updateName(Photo photo, String name) {
		int photoIndex = this.photoAlbum.getPhotos().indexOf(photo);
		photo = this.photoAlbum.getPhotos().get(photoIndex);
		photo.setName(name);
		this.photoListProperty.set(FXCollections.observableArrayList(this.photoAlbum.getPhotos()));
	}
	
	/**
	 * Change filter.
	 *
	 * @param photo the photo
	 * @param filter the filter
	 */
	public void changeFilter(Photo photo, Filter filter) {
		ImageFilter imageFilter = new ImageFilter();
		Image newImage = new Image("file:\\" + photo.getFilePath());
		newImage = imageFilter.applyFilter(newImage, filter);
		this.imageProperty.set(newImage);
	}
	
}
