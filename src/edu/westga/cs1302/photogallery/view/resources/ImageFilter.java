package edu.westga.cs1302.photogallery.view.resources;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * The Class FilterImage.
 * 
 * @author Darrell Branch
 */
public class ImageFilter {

	/**
	 * Instantiates a new filter image.
	 */
	public ImageFilter() {
		
	}
	
	/**
	 * Apply filter.
	 *
	 * @param image the image
	 * @param filterType the filter type
	 * @return the image
	 */
	public Image applyFilter(Image image, Filter filterType) {
		switch (filterType) {
			case NONE:
				return this.applyNormal(image);
			case BLACK_WHITE:
				return this.applyBlackWhite(image);
			case GRAYSCALE:
				return this.applyGrayscale(image);
			case NEGATIVE:
				return this.applyNegative(image);
			default:
				return this.applyNormal(image);
		}
	}
	
	/**
	 * Apply normal.
	 *
	 * @param image the image
	 * @return the image
	 */
	public Image applyNormal(Image image) {
		return image;
	}
	
	/**
	 * Apply black white.
	 *
	 * @param image the image
	 * @return the image
	 */
	public Image applyBlackWhite(Image image) {
		int height = (int) image.getHeight();
		int width = (int) image.getWidth();
		WritableImage newImage = new WritableImage(width, height);
		PixelWriter imageWriter = newImage.getPixelWriter();
		PixelReader imageReader = image.getPixelReader();
		for (int xValue = 0; xValue < width; xValue++) {
			for (int yValue = 0; yValue < height; yValue++) {
		
				Color color = imageReader.getColor(xValue, yValue);
				
				if (color.getBrightness() > 0.50) {
					imageWriter.setColor(xValue, yValue, Color.WHITE);
				} else {
					imageWriter.setColor(xValue, yValue, Color.BLACK);
				}
			}
		}
		
		return newImage;
	}
	
	/**
	 * Apply grayscale.
	 *
	 * @param image the image
	 * @return the image
	 */
	public Image applyGrayscale(Image image) {
		int height = (int) image.getHeight();
		int width = (int) image.getWidth();
		WritableImage newImage = new WritableImage(width, height);
		PixelWriter imageWriter = newImage.getPixelWriter();
		PixelReader imageReader = image.getPixelReader();
		for (int xValue = 0; xValue < width; xValue++) {
			for (int yValue = 0; yValue < height; yValue++) {
		
				Color color = imageReader.getColor(xValue, yValue);
				
				imageWriter.setColor(xValue, yValue, color.grayscale());
			}
		}
		
		return newImage;
	}
	
	/**
	 * Apply negative.
	 *
	 * @param image the image
	 * @return the image
	 */
	public Image applyNegative(Image image) {
		int height = (int) image.getHeight();
		int width = (int) image.getWidth();
		WritableImage newImage = new WritableImage(width, height);
		PixelWriter imageWriter = newImage.getPixelWriter();
		PixelReader imageReader = image.getPixelReader();
		for (int xValue = 0; xValue < width; xValue++) {
			for (int yValue = 0; yValue < height; yValue++) {
		
				Color color = imageReader.getColor(xValue, yValue);
				
				if (color.getBrightness() > 0.50) {
					imageWriter.setColor(xValue, yValue, Color.BLACK);
				} else {
					imageWriter.setColor(xValue, yValue, Color.WHITE);
				}
			}
		}
		
		return newImage;
	}
	
}
