package edu.westga.cs1302.photogallery.view.resources;

/**
 * The Enum Filter.
 * 
 * @author Darrell Branch
 */
public enum Filter {

	NONE,
	/** The black whie. */
	BLACK_WHITE, 
	/** The grayscale. */
	GRAYSCALE,
	/** The negative. */
	NEGATIVE;
	
}
