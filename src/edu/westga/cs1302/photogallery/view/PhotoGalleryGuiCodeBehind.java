package edu.westga.cs1302.photogallery.view;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;

import edu.westga.cs1302.photogallery.model.Photo;
import edu.westga.cs1302.photogallery.view.resources.Filter;
import edu.westga.cs1302.photogallery.viewmodel.PhotoGalleryViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * The Class PhotoGalleryGuiCodeBehind.
 * 
 * @author Darrell Branch
 */
public class PhotoGalleryGuiCodeBehind {

	/** The image view. */
	@FXML
	private ImageView imageView;
	
	/** The list view. */
	@FXML
	private ListView<Photo> listView;
	
	/** The back button. */
	@FXML
	private Button backButton;
	
	/** The next button. */
	@FXML
	private Button nextButton;
	
	/** The remove button. */
	@FXML
	private Button removeButton;
	
	/** The Pane */
	@FXML 
	private Pane thePane;
	
	/** The rename menu item */
	@FXML
	private MenuItem renameMenuItem;
	
	/** The remove menu item */
	@FXML
	private MenuItem removeMenuItem;
	
	/** The remove menu item */
	@FXML
	private ComboBox<Filter> comboBox;
	
	private PhotoGalleryViewModel viewModel;
	
	/**
	 * Instantiates a new photo gallery gui code behind.
	 */
	public PhotoGalleryGuiCodeBehind() {
		this.listView = new ListView<Photo>();
		this.viewModel = new PhotoGalleryViewModel();
		this.comboBox = new ComboBox<Filter>();
	}
	
	/**
	 * Initialize.
	 */
	@FXML
	private void initialize() {
		this.backButton.disableProperty().set(true);
		this.nextButton.disableProperty().set(true);
		this.removeButton.disableProperty().set(true);
		this.listView.itemsProperty().bindBidirectional(this.viewModel.photoListProperty());
		this.setListenersForListView();	
		BooleanBinding renameMenuItemDisable = Bindings.isNull(this.listView.getSelectionModel().selectedItemProperty());
		this.renameMenuItem.disableProperty().bind(renameMenuItemDisable);
		BooleanBinding removeMenuItemDisable = Bindings.isNull(this.listView.getSelectionModel().selectedItemProperty());
		this.removeMenuItem.disableProperty().bind(removeMenuItemDisable);
		this.comboBox.getItems().add(Filter.NONE);
		this.comboBox.getItems().add(Filter.BLACK_WHITE);
		this.comboBox.getItems().add(Filter.GRAYSCALE);
		this.comboBox.getItems().add(Filter.NEGATIVE);
		this.comboBox.getSelectionModel().selectFirst();
		this.imageView.imageProperty().bindBidirectional(this.viewModel.imageProperty());
		this.comboBox.disableProperty().set(true);
	}
	
	@FXML
	private void loadAlbum() {
		Stage stage = (Stage) this.thePane.getScene().getWindow();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Photo Album File");
		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("Photo Album Files", "*.paf"),
				new ExtensionFilter("All Files", "*.*"));
		File selectedFile = fileChooser.showOpenDialog(stage);
		if (selectedFile != null) {
			this.viewModel.clearGallery();
			boolean result = this.viewModel.loadPhotos(selectedFile);	
			if (!result) {
				ArrayList<String> duplicates = this.viewModel.getDuplicates();
				String errorMessage = "Cannot add duplicate photo names: " + System.lineSeparator();
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Error");
				alert.setHeaderText("Error loading photo album");
				
				for (String currString: duplicates) {
					errorMessage += currString;
					errorMessage += System.lineSeparator();
				}
				alert.setContentText(errorMessage);
				alert.showAndWait();
			}
			this.listView.getSelectionModel().select(0);
		
			if (this.listView.itemsProperty().get().size() > 1) {
				this.nextButton.disableProperty().set(false);
				this.backButton.disableProperty().set(false);
				this.removeButton.disableProperty().set(false);
				this.comboBox.disableProperty().set(false);
			}	
		}
	}
	
	@FXML
	private void addPhoto() {
		Stage stage = (Stage) this.thePane.getScene().getWindow();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Add Photo");
		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("Image Files", "*.bmp", "*.gif", "*.jpg", "*.png"),
				new ExtensionFilter("All Files", "*.*"));
		File selectedFile = fileChooser.showOpenDialog(stage);
		
		if (selectedFile != null) {
			this.viewModel.addPhoto(selectedFile);
			this.listView.getSelectionModel().selectLast();
			this.nextButton.disableProperty().set(false);
			this.backButton.disableProperty().set(false);
			this.removeButton.disableProperty().set(false);
			this.comboBox.disableProperty().set(false);
		}
	}
	
	@FXML
	private void savePhotoAlbum() {
		Stage stage = (Stage) this.thePane.getScene().getWindow();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save Photo Album");
		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("Photo Album Files", "*.paf"),
				new ExtensionFilter("All Files", "*.*"));
		File selectedFile = fileChooser.showSaveDialog(stage);
		
		if (selectedFile != null) {
			this.viewModel.saveAlbum(selectedFile);
		}
	}
	
	@FXML
	private void handleAbout() {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("About Photo Gallery");
		alert.setHeaderText("Photo Gallery by Darrell Branch");
		alert.setContentText("Version 1.0");
		alert.showAndWait();
	}
	
	@FXML
	private void handleNextPhotoButton() {
		int gallerySize = this.listView.itemsProperty().get().size();
		int currentPhotoIndex = this.listView.getSelectionModel().getSelectedIndex();
		
		if (currentPhotoIndex != gallerySize - 1) {
			this.listView.getSelectionModel().selectNext();
		} else {
			this.listView.getSelectionModel().selectFirst();
		}
		
		this.comboBox.getSelectionModel().select(Filter.NONE);
		
	}
	
	@FXML
	private void handlePreviousPhotoButton() {
		int currentPhotoIndex = this.listView.getSelectionModel().getSelectedIndex();
		
		if (currentPhotoIndex != 0) {
			this.listView.getSelectionModel().selectPrevious();
		} else {
			this.listView.getSelectionModel().selectLast();
		}
		
		this.comboBox.getSelectionModel().select(Filter.NONE);
	}
	
	@FXML
	private void handleRemoveButton() {
		int indexToRemove = this.listView.getSelectionModel().getSelectedIndex();
		this.handleNextPhotoButton();
		this.viewModel.removePhoto(indexToRemove);
		
		if (this.listView.getSelectionModel().isEmpty()) {
			this.imageView.setImage(null);
			this.removeButton.disableProperty().set(true);
			this.nextButton.disableProperty().set(true);
			this.backButton.disableProperty().set(true);
			this.comboBox.disableProperty().set(true);
		}
	}
	
	@FXML
	private void handleRename() {
		TextInputDialog dialog = new TextInputDialog(this.listView.getSelectionModel().selectedItemProperty().get().getName());
		dialog.setTitle("Rename Photo");
		dialog.setContentText("Enter a new name:");
		Optional<String> result = dialog.showAndWait();
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("Error renaming photo");
		
		if (result.isPresent()) {
			if (!this.isNameUnique(result.get(), this.listView.getSelectionModel().getSelectedItem())) {
				alert.setContentText("Name already assigned to another photo.");
				alert.showAndWait();
			} else if (this.nameHasWhitespace(result.get())) {
				alert.setContentText("No whitespace characters are allowed.");
				alert.showAndWait();
			} else {
				this.viewModel.updateName(this.listView.getSelectionModel().getSelectedItem(), result.get());
			}
		}

	}
	
	@FXML
	private void handleFilterChange() {
		this.viewModel.changeFilter(this.listView.getSelectionModel().getSelectedItem(), this.comboBox.getSelectionModel().getSelectedItem());
	}
	
	private boolean isNameUnique(String name, Photo photo) {
		ObservableList<Photo> allPhotos = this.listView.itemsProperty().get();
		for (Photo currPhoto: allPhotos) {
			if (currPhoto.getName().equals(name)) {
				if (!currPhoto.equals(photo)) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean nameHasWhitespace(String name) {
		if (!name.replaceAll("\\s", "").equals(name)) {
			return true;
		}
		
		return false;
	}
	
	private void setListenersForListView() {
		this.listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		
		this.listView.getSelectionModel().selectedItemProperty().addListener((observable, oldPhoto, newPhoto) -> {
			if (newPhoto != null) {
				this.imageView.setImage(newPhoto.getImage());
			}
		});
	}
	
	
	
}
