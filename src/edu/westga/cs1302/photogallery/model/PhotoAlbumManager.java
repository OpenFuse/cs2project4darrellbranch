package edu.westga.cs1302.photogallery.model;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

/**
 * The Class PhotoAlbumManager.
 * 
 * @author Darrell Branch
 */
public class PhotoAlbumManager implements Collection<Photo> {
	
	/** The photos. */
	private ArrayList<Photo> photos;
	
	private ArrayList<String> duplicateNames;
	
	/**
	 * Instantiates a new photo album manager.
	 */
	public PhotoAlbumManager() {
		this.photos = new ArrayList<Photo>();
		this.duplicateNames = new ArrayList<String>();
	}
	
	/**
	 * Read photo album file.
	 *
	 * @param selectedFile the selected file
	 * 
	 * @return true, if successful
	 */
	public boolean readPhotoAlbumFile(File selectedFile) {
		boolean duplicateFound = false;
		try (Scanner in = new Scanner(selectedFile)) {
			while (in.hasNextLine()) {
				String[] photoData = in.nextLine().split(",");
				Photo newPhoto;
				duplicateFound = false;
				if (photoData.length > 1) {
					newPhoto = new Photo(photoData[0], photoData[1]);
				} else {
					newPhoto = new Photo(photoData[0]);
				}
				for (Photo currPhoto: this.photos) {
					if (currPhoto.getName().equals(newPhoto.getName())) {
						duplicateFound = true;
						if (!this.duplicateNames.contains(newPhoto.getName())) {
							this.duplicateNames.add(newPhoto.getName());
						}
					}
				}
				if (!duplicateFound) {
					this.photos.add(newPhoto);
				}
			}
		} catch (Exception e) {
			System.err.print("error reading file: " + e.getMessage());
		}
		
		return !(this.duplicateNames.size() > 0);
	}
	
	/**
	 * Gets the duplicate names.
	 *
	 * @return the duplicate names
	 */
	public ArrayList<String> getDuplicateNames() {
		return this.duplicateNames;
	}
	
	/**
	 * Save photo album.
	 *
	 * @param selectedFile the selected file
	 */
	public void savePhotoAlbum(File selectedFile) {
		try (PrintWriter out = new PrintWriter(selectedFile)) {
			for (Photo currPhoto: this.photos) {
				out.println(currPhoto.getFilePath() + "," + currPhoto.getName());
			}
		} catch (IOException e) {
			System.err.print(e.getMessage());
		}
	}
	
	/**
	 * Gets the photos.
	 *
	 * @return the photos
	 */
	public ArrayList<Photo> getPhotos() {
		return this.photos;
	}
	
	/** Adds the photo to the collection
	 * 
	 * @param photo the photo to add
	 * 
	 * @precondition none
	 * @postcondition get().size(prev) == get().size() - 1
	 * 
	 * @return true, if successful
	 * 
	 */
	public boolean add(Photo photo) {
		return this.photos.add(photo);
	}
	
	/**
	 * Clears the collection
	 * 
	 * @precondition none
	 * @postcondition get().size() == 0;
	 * 
	 */
	public void clear() {
		this.photos.clear();
		this.duplicateNames.clear();
	}
	
	/**
	 * Checks if the collection is empty
	 * 
	 * @return true, if empty
	 */
	public boolean isEmpty() {
		return this.photos.isEmpty();
	}
	
	/**
	 * Removes the photo
	 *
	 * @param photo the photo
	 * @return true, if successful
	 */
	public boolean remove(Object photo) {
		return this.photos.remove(photo);
	}
	
	/**
	 * Returns the size of the collection
	 * 
	 * @return the size of the collection
	 */
	public int size() {
		return this.photos.size();
	}

	@Override
	public boolean addAll(Collection<? extends Photo> photos) {
		return this.photos.addAll(photos);
	}

	@Override
	public boolean contains(Object photo) {
		return this.photos.contains(photo);
	}

	@Override
	public boolean containsAll(Collection<?> photos) {
		return this.photos.containsAll(photos);
	}

	@Override
	public Iterator<Photo> iterator() {
		return this.photos.iterator();
	}

	@Override
	public boolean removeAll(Collection<?> photos) {
		return this.photos.removeAll(photos);
	}

	@Override
	public boolean retainAll(Collection<?> photos) {
		return this.photos.retainAll(photos);
	}

	@Override
	public Object[] toArray() {
		return this.photos.toArray();
	}

	@Override
	public <T> T[] toArray(T[] photos) {
		return this.photos.toArray(photos);
	}
	
}
