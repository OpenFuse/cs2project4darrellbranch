package edu.westga.cs1302.photogallery.model;

import java.io.File;

import javafx.scene.image.Image;

/**
 * The Class Photo.
 * 
 * @author Darrell Branch
 */
public class Photo {
	
	/** The filename. */
	private String filename;
	
	/** The filePath. */
	private String filePath;
	
	/** The image. */
	private Image image;
	
	/** The name. */
	private String name;
	
	/**
	 * Instantiates a new photo.
	 *
	 * @param filePath the file path
	 */
	public Photo(String filePath) {
		this.filePath = filePath;
		File imageFile = new File(filePath);
		this.filename = imageFile.getName();
		filePath = "file:\\" + filePath;
		this.image = new Image(filePath);
		
		this.name = this.filename.replaceAll("\\s", "");
		this.name = this.name.replaceFirst("[.][^.]+$", "");
	}
	
	/**
	 * Instantiates a new photo.
	 *
	 * @param filePath the file path
	 * @param name the name
	 */
	public Photo(String filePath, String name) {
		this(filePath);
		this.name = name;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		name = name.replaceAll("\\s", "");
		name = name.replaceFirst("[.][^.]+$", "");
		this.name = name;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}
	
	/**
	 * Gets the file path.
	 *
	 * @return the file path
	 */
	public String getFilePath() {
		return this.filePath;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public Image getImage() {
		return this.image;
	}
	
	@Override
	public String toString() {
		return this.getName();
	}

}
